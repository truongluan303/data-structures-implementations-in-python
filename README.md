# Data Structures in Python ![icons8-python](https://user-images.githubusercontent.com/83048295/159117384-14a09c92-1af1-4546-beb8-7a8e9ae8c420.svg)

Data Structures implementations from scratch in Python

### All Data Structures Covered So Far
* Linked List
* Doubly Linked List
* Circular Doubly Linked List
* Linked Queue
* Linked Stack
* Multiset (Bag)
* Binary Search Tree
### Inprogress Implementations
* AVL
* Heap
* Trie
### Testing
In order to test the implementations, `pytest` package is required
Run command `pip install pytest` to install `pytest`
In each folder, you will find a source file for the implementation, and another file to test it. Run `pytest <test-file-name>.py` to test the implementations
